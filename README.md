# Welcome to the Boomi FHIR Connector Open Source Project.

This connector provides a client for doing Create, Update, Query and Delete operations to FHIR resources. It supports STU3 and STU4 versions as well as both JSON and XML resources.

To install the connector to your account, please follow the following instructions.

https://help.boomi.com/bundle/connectors/page/t-atm-Adding_a_connector_group.html

The connector-descriptor.xml and FHIRConnector*.zip files are at the root of this project.
