// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRDeleteOperationTest 
{
    private static final String TEST_TYPE = "Patient";
    private static final String TEST_URL = "http://hapi.fhir.org/baseDstu3";
 
    @Test   
    public void testDeleteOperationNotExists() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("url", TEST_URL);
        
        tester.setOperationContext(OperationType.DELETE, connProps, null, TEST_TYPE, null);
        
        List<String> objectIds = new ArrayList<String>();
        
        objectIds.add("xsxsxsxsxsx");
        
        List <SimpleOperationResult> actual = tester.executeDeleteOperation(objectIds);      
        //This should return 404 but test server returning 200
        assertEquals("200",actual.get(0).getStatusCode());
    }    
}
