package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;
import org.xmlunit.diff.ElementSelectors;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil
{
	public static Map<ObjectDefinitionRole, String> getCookieList(ConnectorCookie cookie, ObjectDefinitionRole role) throws JsonProcessingException
	{
		HashMap<ObjectDefinitionRole, String> map = new HashMap<ObjectDefinitionRole, String>();
		ObjectMapper mapper = new ObjectMapper();
		map.put(role, mapper.writeValueAsString(cookie));
		return map;
	}
	public static void compareXML(String actual, String testName, Class theClass, boolean writeExpected) throws Exception
	{
        System.out.println("");
        System.out.println(testName);
		if (writeExpected)
		{
			FileWriter writer = new FileWriter("src/test/java/resources/expected/"+testName+".xml");
			writer.write(actual);
			writer.flush();
			writer.close();
		}
        String expected = readResource("resources/expected/"+testName+".xml", theClass);
//        System.out.println(expected);       
//        System.out.println(actual);       

	
       Diff myDiffSimilar;
       myDiffSimilar = DiffBuilder.compare(expected).withTest(actual)
	     .withNodeMatcher(new DefaultNodeMatcher(ElementSelectors.byName))
	     .checkForSimilar().ignoreWhitespace()
	     .build();
//       	System.out.println("actual: " + actual.length() + " expected: " + expected.length());
//        System.out.println(myDiffSimilar.toString());
        for (Difference dif: myDiffSimilar.getDifferences())
        {
            System.out.println("DIFFERENCE");
            System.out.println(dif.toString());
        }

        assertTrue(!myDiffSimilar.hasDifferences());
//        System.out.println(myDiffSimilar.toString());
        assertTrue(!myDiffSimilar.hasDifferences());
        assertEquals(actual.length(), expected.length());
	}
	
    static String inputStreamToString(InputStream is) throws IOException
    {
    	try (Scanner scanner = new Scanner(is, "UTF-8")) {
    		return scanner.useDelimiter("\\A").next();
    	}
    }

	static String readResource(String resourcePath, Class theClass) throws Exception
	{
		String resource = null;
		try {
			InputStream is = theClass.getClassLoader().getResourceAsStream(resourcePath);
			resource = inputStreamToString(is);
			
		} catch (Exception e)
		{
			throw new Exception("Error loading resource: "+resourcePath + " " + e.getMessage());
		}

		return resource;
	}

//	public boolean equals(Object o)
//	{
//	    if (o == this) return true;
//	    if (o == null) return false;
//	    if (o.getClass() != getClass()) {
//	        return false;
//	    }
//	    ObjectNode other = (ObjectNode) o;
//	    if (other.size() != size()) {
//	        return false;
//	    }
//	    if (_children != null) {
//	        for (Map.Entry<String, JsonNode> en : _children.entrySet()) {
//	            String key = en.getKey();
//	            JsonNode value = en.getValue();
//
//	            JsonNode otherValue = other.get(key);
//
//	            if (otherValue == null || !otherValue.equals(value)) {
//	                return false;
//	            }
//	        }
//	    }
//	    return true;
//	}
}