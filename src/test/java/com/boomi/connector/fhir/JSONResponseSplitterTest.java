package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.util.IOUtil;

class JSONResponseSplitterTest {

	void testJSONResponseSplitter() throws IOException {
        JSONResponseSplitter respSplitter = null;
        try {

            // split "list" result document into multiple payloads and process each as a partial result
            respSplitter = new JSONResponseSplitter(this.getClass().getClassLoader().getResourceAsStream("resources/mockQueryResponse.json"));

            for(Payload p : respSplitter) 
            {
//                ResponseUtil.addPartialSuccess(response, input, resp.getResponseCodeAsString(), p);
//            	p.readFrom().close();
            }

            String nextOffset = respSplitter.getNextPageURL();           
        } finally {
            IOUtil.closeQuietly(respSplitter);
        }

	}

}
