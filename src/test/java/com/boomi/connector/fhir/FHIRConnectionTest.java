// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.fhir.FHIRConnector.AuthType;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.HistoryType;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRConnectionTest 
{
    // google app returns microseconds, so pad millis

    private static final String TEST_TYPE = "ValueSet";
    private static final String LOINC_URL = "https://fhir.loinc.org";
    
    @Test   
    public void testBasicAuth401() throws Exception
    {
    	//Doing a negative 401 test to avoid using a real password
    	FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), LOINC_URL);
        connProps.put(ConnectionPropertyIds.username.name(), "djwocky");
        connProps.put(ConnectionPropertyIds.password.name(), "fakepassword");
        connProps.put(ConnectionPropertyIds.authenticationType.name(), AuthType.basic.name());

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.history.name(), HistoryType.none.name());
        
        tester.setOperationContext(OperationType.QUERY, connProps, opProps, TEST_TYPE, null);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        assertEquals("401",actual.get(0).getStatusCode());
    }
}
