// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

/**
 * @author Dave Hock
 */
public class FHIRUtilJSONTest 
{
	@Test
	public void testResolveObjectSchema() throws Exception
	{
		List<String> excludes = new ArrayList<String>();
		excludes.add("extension");

		JSONObject schema = FHIRUtilJSON.getJSONSchema(getClass(), "STU4");
//		FHIRUtilJSON.resolveObjectSchema(sbSchema, schema, "Patient", 0);
		StringBuilder sbActual=new StringBuilder();
		FHIRUtilJSON.resolveObjectSchema(sbActual, schema, "Patient", 1, excludes);
        String expected = TestUtil.readResource("resources/expectedTestResolveObjectSchema.json", getClass());
        String actual = sbActual.toString();
//		BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/java/resources/expectedTestResolveObjectSchema.json"));
//    	writer.write(actual);
//    	writer.close();
//        System.out.println(actual);
//        System.out.println(expected);
        assertEquals(expected, actual);        
	}
	@Test
	public void testResolveObjectSchemaDeep() throws Exception
	{
		List<String> excludes = new ArrayList<String>();
		excludes.add("extension");
		JSONObject schema = FHIRUtilJSON.getJSONSchema(getClass(), "STU4");
		StringBuilder sbActual=new StringBuilder();
		FHIRUtilJSON.resolveObjectSchema(sbActual, schema, "Patient", 8, excludes);
        
        String actual = sbActual.toString();
        String expected = TestUtil.readResource("resources/expectedTestResolveObjectSchemaDeep.json", getClass());
//        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/java/resources/expectedTestResolveObjectSchemaDeep.json"));
//      	writer.write(actual);
//      	writer.close();
//        System.out.println(actual);
//        System.out.println(expected);
        assertEquals(expected, actual);
//        System.out.println(validSchema.toString());
	}
}