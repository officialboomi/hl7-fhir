// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.fhir.FHIRConnector.AuthType;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.HistoryType;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRUpsertOperationTest 
{
    private static final String TEST_TYPE = "Patient";
    private static final String TEST_URL = "http://hapi.fhir.org/baseDstu3";
    
    @Test
    public void testUpdateOperationJSON() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), TEST_URL);
        opProps.put(OperationPropertyIds.replace.name(), true);
        
        tester.setOperationContext(OperationType.UPSERT, connProps, opProps, TEST_TYPE, null);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
//TODO Where do we set the objectId???            	
        
        inputs.add(getClass().getClassLoader().getResourceAsStream("resources/patient-example-f001-pieter.json"));
        
        List<SimpleOperationResult> actual = tester.executeUpsertOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200", actual.get(0).getStatusCode());

    }

    @Test
    public void testUpdateOperationXML() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), TEST_URL);
        opProps.put(OperationPropertyIds.replace.name(), true);
        opProps.put(OperationPropertyIds.format.name(), Format.xml);
         
        tester.setOperationContext(OperationType.UPSERT, connProps, opProps, TEST_TYPE, null);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
//TODO Where do we set the objectId???            	
        
        inputs.add(getClass().getClassLoader().getResourceAsStream("resources/patient-example-f001-pieter.xml"));
        
        List<SimpleOperationResult> actual = tester.executeUpsertOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200", actual.get(0).getStatusCode());

    }
}