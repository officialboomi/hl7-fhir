// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRCreateOperationTest 
{
    private static final String TEST_TYPE = "Patient";
    private static final String TEST_URL = "http://hapi.fhir.org/baseDstu3";
    
    @Test   
    public void testCreateOperationJSON() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.json.name());        
        
        tester.setOperationContext(OperationType.CREATE, connProps, opProps, TEST_TYPE, null);
        
        List<InputStream> inputs = new ArrayList<InputStream>();
        
        inputs.add(getClass().getClassLoader().getResourceAsStream("resources/patient-example-f001-pieter.json"));
        
        List<SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("Created", actual.get(0).getMessage());
        assertEquals("201", actual.get(0).getStatusCode());
        
    }    
}
