// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector.fhir;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.fhir.FHIRConnector.AuthType;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.EditResponseType;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.StuVersion;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author Dave Hock
 */
public class FHIRBrowserTest {

    private static final String HAPI_STU3_URL = "http://hapi.fhir.org/baseDstu3";
    private static final String LOINC_URL = "https://fhir.loinc.org";
    private static final String HAPI_STU4_URL = "http://hapi.fhir.org/baseR4";

//    //Bummer  Browser.testConnection not supported by tester
//	@Disabled
//	@Test
//	public void testConnection() throws Exception
//	{
//        FHIRConnector connector = new FHIRConnector();
//        ConnectorTester tester = new ConnectorTester(connector);
//        tester.getOperationContext().
//	}
	
    public void testLOINC_URLBrowseObjectTypes() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(ConnectionPropertyIds.url.name(), LOINC_URL);
        connProps.put(ConnectionPropertyIds.authenticationType.name(), AuthType.basic.name());
        connProps.put(ConnectionPropertyIds.username.name(), "");
        connProps.put(ConnectionPropertyIds.password.name(), "");
        
        tester.setBrowseContext(OperationType.GET, connProps, null);
        actual = tester.browseTypes();       
        TestUtil.compareXML(actual, "testLOINC_URLBrowseObjectTypes", getClass(), false);
    }
    
    @Test
    public void testHAPI_STU3_URLBrowseObjectTypes() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        
        tester.setBrowseContext(OperationType.GET, connProps, null);
        actual = tester.browseTypes();       
        TestUtil.compareXML(actual, "testHAPI_STU3_URLBrowseObjectTypes", getClass(), false);
    }

    @Test
    public void testGetJSON4DeepBrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.json.name());
        opProps.put(OperationPropertyIds.maxDepth.name(), "4");
//        opProps.put(OperationPropertyIds.stuVersion.name(), StuVersion.STU3.name());

        tester.setBrowseContext(OperationType.GET, connProps, opProps);
        
        String actual = tester.browseProfiles("Patient");        
        TestUtil.compareXML(actual, "testGetJSON4DeepBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testGetJSON6DeepBrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.json.name());
        opProps.put("maxDepth", "6");

        tester.setBrowseContext(OperationType.GET, connProps, opProps);
        
        String actual = tester.browseProfiles("Patient");
        TestUtil.compareXML(actual, "testGetJSON6DeepBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testGetXMLBrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
 
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        opProps.put(OperationPropertyIds.format.name(), Format.xml.name());

        tester.setBrowseContext(OperationType.GET, connProps, opProps);
        
        String actual = tester.browseProfiles("Patient");
//NOTE we removed the fhir-base.xsd document elements because it was breaking compare 
//Expected text value 'RFC 3001. See also ISO/IEC 8824:1990 ?' but was 'RFC 3001. See also ISO/IEC 8824:1990 �' - comparing <xs:documentation ...>RFC 3001. See also ISO/IEC 8824:1990 ?</xs:documentation> at /ObjectDefinitions[1]/definition[1]/extraSchema[1]/schema[1]/complexType[6]/annotation[1]/documentation[2]/text()[1] to <xs:documentation ...>RFC 3001. See also ISO/IEC 8824:1990 �</xs:documentation> at /ObjectDefinitions[1]/definition[1]/extraSchema[1]/schema[1]/complexType[6]/annotation[1]/documentation[2]/text()[1] (DIFFERENT)

        TestUtil.compareXML(actual, "testGetXMLBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testQueryXMLBrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.xml.name());

        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);

        String actual = tester.browseProfiles("Patient");
        TestUtil.compareXML(actual, "testQueryXMLBrowseObjectDefinitions", getClass(), false);
     }
    
    @Test
    public void testQueryJSONBrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.json.name());

        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);

//    	String actual = getObjectDefinition(HAPI_STU3_URL, Format.json.name(), StuVersion.STU4.name(), OperationType.QUERY, "Patient", "5", EditResponseType.representation.name());        
        String actual = tester.browseProfiles("Patient");
        TestUtil.compareXML(actual, "testQueryJSONBrowseObjectDefinitions", getClass(), false);
     }
    
    @Test
    public void testQueryXMLSTU3BrowseObjectDefinitions() throws Exception {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), HAPI_STU3_URL);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), Format.xml.name());
        opProps.put(OperationPropertyIds.stuVersion.name(), StuVersion.STU3.name());

        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);

    	String actual = getObjectDefinition(HAPI_STU3_URL, Format.xml.name(), StuVersion.STU3.name(), OperationType.QUERY, "Patient", "5", EditResponseType.representation.name());        
//        String actual = tester.browseProfiles("Patient");
        TestUtil.compareXML(actual, "testQueryXMLSTU3BrowseObjectDefinitions", getClass(), false);
     }
    
    @Test
    public void testQueryJSONR5BrowseObjectDefinitions() throws Exception {        
    	String actual = getObjectDefinition(HAPI_STU3_URL, Format.json.name(), StuVersion.R5.name(), OperationType.QUERY, "Patient", "5", EditResponseType.representation.name());
        TestUtil.compareXML(actual, "testQueryJSONR5BrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testQueryJSONSTU3BrowseObjectDefinitions() throws Exception {        
    	String actual = getObjectDefinition(HAPI_STU3_URL, Format.json.name(), StuVersion.STU3.name(), OperationType.QUERY, "Patient", "5", EditResponseType.representation.name());
        TestUtil.compareXML(actual, "testQueryJSONSTU3BrowseObjectDefinitions", getClass(), false);
     }
    
    @Test
    public void testCreateMinimalXMLBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU4_URL, Format.xml.name(), StuVersion.STU4.name(), OperationType.CREATE, "Patient", "6", EditResponseType.minimal.name());
        TestUtil.compareXML(actual, "testCreateMinimalXMLBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testUpdateMinimalXMLBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU4_URL, Format.xml.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.minimal.name());
        TestUtil.compareXML(actual, "testUpdateMinimalXMLBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testUpdateOperationOutcomeXMLBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU4_URL, Format.xml.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.OperationOutcome.name());
        TestUtil.compareXML(actual, "testUpdateOperationOutcomeXMLBrowseObjectDefinitions", getClass(), false);
    }

    @Test
    public void testUpdateRepresentationXMLBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU3_URL, Format.xml.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.representation.name());
        TestUtil.compareXML(actual, "testUpdateRepresentationXMLBrowseObjectDefinitions", getClass(), false);
    }
    @Test
    public void testUpdateMinimalJSONBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU3_URL, Format.json.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.minimal.name());
        TestUtil.compareXML(actual, "testUpdateMinimalJSONBrowseObjectDefinitions", getClass(), false);
    }
    
    @Test
    public void testUpdateOperationOutcomeJSONBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU4_URL, Format.json.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.OperationOutcome.name());
        TestUtil.compareXML(actual, "testUpdateOperationOutcomeJSONBrowseObjectDefinitions", getClass(), false);
    }

    @Test
    public void testUpdateRepresentationJSONBrowseObjectDefinitions() throws Exception {
        String actual = getObjectDefinition(HAPI_STU4_URL, Format.json.name(), StuVersion.STU4.name(), OperationType.UPDATE, "Patient", "6", EditResponseType.representation.name());
        TestUtil.compareXML(actual, "testUpdateRepresentationJSONBrowseObjectDefinitions", getClass(), false);
    }
    
    public void testAllOperations() throws Exception
    {
    	
    }
    
    public String getObjectDefinition(String url, String format, String stuVersion, OperationType operation, String resource, String maxDepth, String outcome) throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), url);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.format.name(), format);
        if (outcome!=null)
        	opProps.put(OperationPropertyIds.editResponseType.name(), outcome);
        if (stuVersion!=null)
        	opProps.put(OperationPropertyIds.stuVersion.name(), stuVersion);
        opProps.put("maxDepth", maxDepth);

        tester.setBrowseContext(operation, connProps, opProps);
    	return tester.browseProfiles(resource);
    }
}
