// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.fhir.FHIRConnector.HistoryType;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRGetOperationTest 
{
    private static final String TEST_TYPE = "Patient";
    private static final String TEST_URL = "http://hapi.fhir.org/baseDstu3";

    @Test   
    public void testGetOperationNoHistoryXML() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("url", TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("history", HistoryType.none.name());
        
        ConnectorCookie cookie = new ConnectorCookie();
        cookie.setFormat("xml");
        
        tester.setOperationContext(OperationType.GET, connProps, opProps, TEST_TYPE, TestUtil.getCookieList(cookie, ObjectDefinitionRole.OUTPUT));
        List <SimpleOperationResult> actual = tester.executeGetOperation("2506693");
//        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());

//        tester.testExecuteGetOperation("1669073", Arrays.asList(
//                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(FHIRUtil.readResource("resources/expectedGetResponse.json", getClass()).getBytes()))));
    }
 
    //Get doesn't support GET with no id
    public void testGetOperationResourceHistoryXML() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("url", TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("history", HistoryType.resource.name());
        
        long recordCount = 42L;
        opProps.put(OperationPropertyIds.maxDocuments.name(), recordCount);
        
        ConnectorCookie cookie = new ConnectorCookie();
        cookie.setFormat("xml");
        
        tester.setOperationContext(OperationType.GET, connProps, opProps, TEST_TYPE, TestUtil.getCookieList(cookie, ObjectDefinitionRole.OUTPUT));
        List <SimpleOperationResult> actual = tester.executeGetOperation("2506693");
//        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(recordCount, actual.get(0).getPayloads().size());

//        tester.testExecuteGetOperation("1669073", Arrays.asList(
//                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList(FHIRUtil.readResource("resources/expectedGetResponse.json", getClass()).getBytes()))));
    }
    
    @Test   
    public void testGetOperationWithRecordHistoryXML() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("url", TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationPropertyIds.history.name(), HistoryType.record.name());

        ConnectorCookie cookie = new ConnectorCookie();
        cookie.setFormat("xml");

        tester.setOperationContext(OperationType.GET, connProps, opProps, TEST_TYPE, TestUtil.getCookieList(cookie, ObjectDefinitionRole.OUTPUT));

        List <SimpleOperationResult> actual = tester.executeGetOperation("1669073");
        assertEquals(OperationStatus.SUCCESS, actual.get(0).getStatus());
        assertEquals(null,actual.get(0).getMessage());
        assertEquals(1, actual.get(0).getPayloads().size());
        
        //Negative Test
        actual = tester.executeGetOperation("XX1669073");
        assertEquals("Not Found", actual.get(0).getMessage());
        assertEquals("404",actual.get(0).getStatusCode());
//TODO Should a 404 on a get return a document???        
        assertEquals(1, actual.get(0).getPayloads().size());
//        tester.("1669073", Arrays.asList(
//                                               new SimpleOperationResult(OperationStatus.SUCCESS, "200", "OK", Arrays.asList("".getBytes()))));
    }    
}
