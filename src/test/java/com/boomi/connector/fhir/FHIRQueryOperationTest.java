// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector.fhir;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class FHIRQueryOperationTest 
{
    private static final String TEST_TYPE = "Patient";
    private static final String TEST_URL = "http://hapi.fhir.org/baseR4";
    
    @Test   
    public void testSimpleQueryOperationJSON() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        long maxPages = 3L;
        long pageSize = 5; 
        opProps.put(OperationPropertyIds.maxDocuments.name(), maxPages);
        opProps.put(OperationPropertyIds.batchSize.name(), pageSize);
        ConnectorCookie cookie = new ConnectorCookie();
        cookie.setFormat("json");
      
//        tester.setOperationContext(OperationType.QUERY, connProps, opProps, TEST_TYPE, null);
        tester.setOperationContext(OperationType.QUERY, connProps, opProps, TEST_TYPE, TestUtil.getCookieList(cookie, ObjectDefinitionRole.OUTPUT));
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("gender", "eq", "male"),
//                new QuerySimpleBuilder("given", "eq", "Niel"),              
                new QuerySimpleBuilder("_lastUpdated", "ge", "2019-09-01"))).toFilter();
        Sort sort1 = new Sort();
        sort1.setSortOrder("desc");
        sort1.setProperty("given");
//        Sort sort2 = new Sort();
//        sort2.setSortOrder("asc");
//        sort2.setProperty("gender");
//        qf.withSort(sort1, sort2);//2 sorts throwing 500, 
//        qf.withSort(sort1); //sort not working returns <issue><diagnostics value="ERROR: canceling statement due to user request"/>
        List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        String actualString = new String(actual.get(0).getPayloads().get(0));
        
		FileWriter writer = new FileWriter("src/test/java/resources/expected/testSimpleQueryOperationJSON.json");
		writer.write(actualString);
		writer.flush();
		writer.close();

        assertEquals(OperationStatus.SUCCESS, actual.get(0).getStatus());
        assertEquals(null,actual.get(0).getMessage());
        assertEquals(maxPages*pageSize,actual.get(0).getPayloads().size());
//        System.out.println(actual.get(0).getPayloads().size()+"");

//        tester.testExecuteQueryOperation(qf, new ArrayList<SimpleOperationResult>());
    }
    
    @Test   
    public void testSimpleQueryOperationXML() throws Exception
    {
        FHIRConnector connector = new FHIRConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(ConnectionPropertyIds.url.name(), TEST_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        long maxPages = 3L;
        long pageSize = 100; 
        opProps.put(OperationPropertyIds.maxDocuments.name(), maxPages);
        opProps.put(OperationPropertyIds.batchSize.name(), pageSize);
        
        ConnectorCookie cookie = new ConnectorCookie();
        cookie.setFormat("xml");

        tester.setOperationContext(OperationType.QUERY, connProps, opProps, TEST_TYPE, TestUtil.getCookieList(cookie, ObjectDefinitionRole.OUTPUT));
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("gender", "eq", "male"),
                new QuerySimpleBuilder("_lastUpdated", "ge", "2019-08-01"))).toFilter();
        Sort sort1 = new Sort();
        sort1.setSortOrder("desc");
        sort1.setProperty("given");
//        qf.withSort(sort1); //2 sorts throwing 500, sort not working returns <issue><diagnostics value="ERROR: canceling statement due to user request"/>
        List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        assertEquals(OperationStatus.SUCCESS, actual.get(0).getStatus());
        assertEquals(null,actual.get(0).getMessage());
//        System.out.println(actual.get(0).getPayloads().size()+"");
        assertEquals(maxPages*pageSize,actual.get(0).getPayloads().size());

//        tester.testExecuteQueryOperation(qf, new ArrayList<SimpleOperationResult>());
    }
    
}
