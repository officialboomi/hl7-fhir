package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.TrackedData;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class FHIRUtilXML
{	
    static void processBatchResponse(String nextPageURL, FHIRConnection connection, OperationResponse opResponse, TrackedData input, PropertyMap opProps, ConnectorCookie cookie, Logger logger) throws XMLStreamException, IOException
    {
        // process all the results, handling batching
        int numPages = 0;
        do {            
           	logger.log(Level.INFO, "Document index:" + numPages);
           	FHIRResponse resp = connection.doGet(nextPageURL, cookie);
                        
            if (resp.getStatus() != OperationStatus.SUCCESS) {
                // just dump the result and bailout, nothing more we can do
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {

                        opResponse.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                        		resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                    	opResponse.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), null);
                    }
                }
                finally {
                    IOUtil.closeQuietly(obj);
                }
                break;
            }

            XMLResponseSplitter respSplitter = null;
            try {

                // split "list" result document into multiple payloads and process each as a partial result
                respSplitter = new XMLResponseSplitter(resp.getResponse());

                for(Payload p : respSplitter) {
                    ResponseUtil.addPartialSuccess(opResponse, input, resp.getResponseCodeAsString(), p);
                }

                nextPageURL = respSplitter.getNextPageURL();
                
            } finally {
                IOUtil.closeQuietly(respSplitter);
            }
            numPages++; //actually max pages because if we break out of for(Payload, IOUtil.closeQuietly locks the main thread
        } while(!StringUtil.isBlank(nextPageURL) && numPages < connection.getMaxDocuments(opProps));

        // indicate that we are finished adding all the results
        opResponse.finishPartialResult(input);
    }    
 }