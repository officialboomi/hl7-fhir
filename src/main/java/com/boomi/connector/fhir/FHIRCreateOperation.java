package com.boomi.connector.fhir;

import java.io.InputStream;
import java.util.logging.Logger;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;

public class FHIRCreateOperation extends BaseUpdateOperation {

	protected FHIRCreateOperation(FHIRConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
    	Logger logger = response.getLogger();
        String objectType = getContext().getObjectTypeId();
        PropertyMap opProps = this.getContext().getOperationProperties();
        ConnectorCookie cookie = FHIRUtil.getInputCookie(getContext());

        for (ObjectData input : request) {

            try {
                // POST the content to the create url
                FHIRResponse resp = getConnection().doCreate(objectType, "", input.getData(), opProps, cookie);

                // dump the results into the response
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {
                        response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                        response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage());
                    }

                }
                finally {
                    IOUtil.closeQuietly(obj);
                }

            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
	}

	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}