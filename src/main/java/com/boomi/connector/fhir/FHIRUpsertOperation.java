package com.boomi.connector.fhir;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

public class FHIRUpsertOperation extends BaseUpdateOperation {

	protected FHIRUpsertOperation(FHIRConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		FHIRUpdateOperation.doUpdate(request, response, getContext(), getConnection());
	}

	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}