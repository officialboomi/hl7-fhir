/* Copyright � 2020 Boomi Inc. or its subsidiaries. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ExtraSchema;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.fhir.FHIRConnector.EditResponseType;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.StuVersion;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.DOMUtil;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.fge.jsonschema.main.JsonSchema;
//import com.github.fge.jsonschema.main.JsonSchemaFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException; 

/**
 * FHIRBrowser operation implementation.
 * 
 * @author Dave Hock
 */
public class FHIRBrowser extends BaseBrowser implements ConnectionTester {
	Logger logger = Logger.getLogger("FHIRBrowser");
    protected FHIRBrowser(FHIRConnection conn) {
        super(conn);
    }
    
	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
		
	@Override
	public void testConnection() {
		try {
			this.getConnection().testConnection();
        }
        catch (Exception e) {
            throw new ConnectorException("Could not establish a connection", e);
        }
	}

	@Override
	public ObjectTypes getObjectTypes() {
		
		ObjectTypes types=null;
		try {
			types = new ObjectTypes();
//			types = readTypesFromResource();
			types = readResourceTypesFromServer();
		} 
	    catch (Exception e) {
	    	throw new ConnectorException(e);
	    }

        return types;
	}
	
	private ObjectTypes readResourceTypesFromServer() throws Exception
	{
		ObjectTypes types = new ObjectTypes();
		JSONObject root = this.getConnection().getMetadata();
		JSONArray resources = root.getJSONArray("rest").getJSONObject(0).getJSONArray("resource");
		for (int x=0; x<resources.length(); x++)
		{
		    ObjectType type = new ObjectType();
		    JSONObject resource = resources.getJSONObject(x);
		    if (resource.has("interaction"))
		    {
			    JSONArray interactions = resource.getJSONArray("interaction");
			    //TODO week attempt to exclude non-readable resources like those at loinc
			    if (interactions.length()>2 && resources.getJSONObject(x).has("type"))
			    {
					String typeName = resources.getJSONObject(x).getString("type");
				    type.setId(typeName);
				    types.getTypes().add(type);
			    }
		    }
		}
		return types;
	}
	
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
		Collection<ObjectDefinitionRole> roles) {
		
		ConnectorCookie cookie = new ConnectorCookie();
		ObjectMapper mapper=new ObjectMapper();
		mapper.disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);
		
		OperationType operationType = getContext().getOperationType();
		ObjectDefinitions objDefinitions = new ObjectDefinitions();
		ObjectDefinition objDefinition = new ObjectDefinition();
		
		ObjectDefinition objDefinitionOutcome = new ObjectDefinition();
		ContentType contentType;

		PropertyMap opProps = this.getContext().getOperationProperties();
		
		String format=opProps.getProperty(OperationPropertyIds.format.name(), Format.json.name());
		cookie.setFormat(format);
		String editResponseType = opProps.getProperty(OperationPropertyIds.editResponseType.name(), EditResponseType.minimal.name());
		boolean excludeExtensions = opProps.getBooleanProperty(OperationPropertyIds.excludeExtensions.name(), true);
		List<String> excludes = new ArrayList<String>();
		if (excludeExtensions)
			excludes.add("extension");

		
		cookie.setRepresentation(editResponseType);
		String stuVersion = opProps.getProperty(OperationPropertyIds.stuVersion.name(), StuVersion.R5.name());
		cookie.setStuVersion(stuVersion);
		
		try {
			
			if (Format.json.name().contentEquals(format))
			{
				long maxDepth=Long.parseLong(opProps.getProperty(OperationPropertyIds.maxDepth.name(), "5"));
				contentType = ContentType.JSON;
				
				populateJSONSchema(objectTypeId, objDefinition, maxDepth, stuVersion, excludes);
				if (editResponseType.contentEquals(EditResponseType.OperationOutcome.name()))
				{
					populateJSONSchema("OperationOutcome", objDefinitionOutcome, maxDepth, stuVersion, excludes);
				} else if (editResponseType.contentEquals(EditResponseType.representation.name()))
				{
					populateJSONSchema(objectTypeId, objDefinitionOutcome, maxDepth, stuVersion, excludes);
				}
			}
			else
			{
				contentType = ContentType.XML;
				populateXMLSchema(objectTypeId,objDefinition, stuVersion);				
				//TODO All xsd's breaks import because too big
				//populateXMLExtraSchemas(objectTypeId, objDefinition);			
				//Instead just load the first level of include
				populateXMLExtraSchema("fhir-base.xsd", objDefinition, stuVersion);
				//We need another level because of xhtml:div tag in text
				populateXMLExtraSchema("fhir-xhtml.xsd", objDefinition, stuVersion);
				populateXMLExtraSchema("xml.xsd", objDefinition, stuVersion);	
				
				if (editResponseType.contentEquals(EditResponseType.OperationOutcome.name()))
				{
					populateXMLSchema("OperationOutcome", objDefinitionOutcome, stuVersion);
					populateXMLExtraSchema("fhir-base.xsd", objDefinitionOutcome, stuVersion);
					populateXMLExtraSchema("fhir-xhtml.xsd", objDefinitionOutcome, stuVersion);
					populateXMLExtraSchema("xml.xsd", objDefinitionOutcome, stuVersion);				
				} else if (editResponseType.contentEquals(EditResponseType.representation.name()))
				{
					populateXMLSchema(objectTypeId,objDefinitionOutcome, stuVersion);				
					//TODO All xsd's breaks import because too big
					//populateXMLExtraSchemas(objectTypeId, objDefinitionOutcome);			
					//Instead just load the first level of include
					populateXMLExtraSchema("fhir-base.xsd", objDefinitionOutcome, stuVersion);
					//We need another level because of xhtml:div tag in text
					populateXMLExtraSchema("fhir-xhtml.xsd", objDefinitionOutcome, stuVersion);
					populateXMLExtraSchema("xml.xsd", objDefinitionOutcome, stuVersion);	
				}
			}

			objDefinitionOutcome.setOutputType(contentType);
			
			for(ObjectDefinitionRole role : roles)
			{
				if (ObjectDefinitionRole.INPUT == role)
				{
					switch (operationType)
					{
					case UPDATE:
					case CREATE:
					case UPSERT:
						objDefinition.setInputType(contentType);
						objDefinitions.getDefinitions().add(objDefinition);								
						break;
					default:
						break;
					}			
					objDefinition.setCookie(mapper.writeValueAsString(cookie));
					
				} else if (ObjectDefinitionRole.OUTPUT == role)
				{
					
					switch (operationType)
					{
					case GET:
						objDefinition.setOutputType(contentType);
						objDefinitions.getDefinitions().add(objDefinition);		
						break;
					case QUERY:
						objDefinition.setOutputType(contentType);
						
						List<FieldSpecField> fieldSpecFields = new ArrayList<FieldSpecField>();
						//Do this first as we will tag selectable on to existing filterable/sortable
						populateQueryFilterSortFields(fieldSpecFields, objectTypeId);
						int numSelectedableFields = populateQuerySelectableFields(fieldSpecFields, objectTypeId, stuVersion);
						sortFieldSpecFields(fieldSpecFields);				
						for (FieldSpecField fieldSpecField : fieldSpecFields)
						{
							objDefinition.getFieldSpecFields().add(fieldSpecField);
						}
						cookie.setTotalNumberFields(numSelectedableFields);
						//Set the cookie for QueryOperation so we know how many fields there are and can compare with getSelectedFields().size() to see if all are selected and avoid using _elements= in that case
						objDefinitions.getDefinitions().add(objDefinition);			
						break;
					case UPDATE:
					case CREATE:
					case UPSERT:
						if (!editResponseType.contentEquals(EditResponseType.minimal.name()))
						{
							objDefinitions.getDefinitions().add(objDefinitionOutcome);			
						}
						break;
					default:
						break;
					}			
				}
			}			
			objDefinition.setCookie(mapper.writeValueAsString(cookie));
		} 
	    catch (Exception e) {
	    	throw new ConnectorException(e);
	    }
        return objDefinitions;
	}
	
	//Add schema to the object definition
	private void populateXMLSchema(String objectTypeId, ObjectDefinition objDefinition, String stuVersion) throws Exception
	{
		String schemaFileName = objectTypeId.toLowerCase()+".xsd";
        InputStream is = FHIRUtil.getResourceAsStream("resources/"+stuVersion+"/xsd/"+schemaFileName, getClass());
        try {
            Document doc = DOMUtil.newDocumentBuilderNS().parse(is);
            objDefinition.setElementName(objectTypeId);            
            objDefinition.setSchema(doc.getDocumentElement());
        }
	    catch (Exception e) {
	    	throw new ConnectorException(e);
	    }
        finally {
            IOUtil.closeQuietly(is);
        }      
	}
	
//	private void populateXMLExtraSchemas(String objectTypeId, ObjectDefinition objDefinition) throws IOException, SAXException
//	{
//		String schemaFileName = objectTypeId.toLowerCase()+".xsd";
//		InputStream isAllXSD = getClass().getClassLoader().getResourceAsStream("resources/xsd/allxsd.txt");
//        try {
//    		BufferedReader br =  new BufferedReader(new InputStreamReader(isAllXSD));
//    		String line;
//    		while ((line = br.readLine()) != null) {
//    			if (!schemaFileName.contentEquals(line))
//    			{
//                    //.safeGetExtraSchemas().withSchemas(doc.getDocumentElement());
//    				populateAnExtraSchema(line, objDefinition);
//    			}
//    		}
//    		br.close();
//
// 			objDefinition.safeGetExtraSchemas();
//        } 
//	    catch (Exception e) {
//	    	throw new ConnectorException(e);
//	    }
//	    finally {
//        	IOUtil.closeQuietly(isAllXSD);
//	    }        
//	}
	
	private void populateXMLExtraSchema(String schemaFileName, ObjectDefinition objDefinition, String stuVersion) throws SAXException, IOException
	{
		InputStream isXSD = getClass().getClassLoader().getResourceAsStream("resources/"+stuVersion+"/xsd/"+schemaFileName);
        try {
    		Document doc = DOMUtil.newDocumentBuilderNS().parse(isXSD);
            ExtraSchema extraSchema = new ExtraSchema();
            extraSchema.setSchema(doc.getDocumentElement());
            extraSchema.setSystemId(schemaFileName);
            objDefinition.getExtraSchemaList().add(extraSchema);		
        }
	    catch (Exception e) {
	    	throw new ConnectorException(e);
	    }
        finally {
        	IOUtil.closeQuietly(isXSD);
        }
	}
	
	//Add schema to the object definition
	private void populateJSONSchema(String objectTypeId, ObjectDefinition objDefinition, long maxDepth, String stuVersion, List<String> excludes) throws Exception
	{
		//added "type":"string" to types with "enum" and "const"
		//Got rid of circular reference by change Indentifier.assigner to a string
		JSONObject schema = FHIRUtilJSON.getJSONSchema(getClass(), stuVersion);
		//This approach resolves the $refs to a limited depth schema with no $refs
		StringBuilder sbSchema = new StringBuilder();
		//simplepatientschema.json
		FHIRUtilJSON.resolveObjectSchema(sbSchema, schema, objectTypeId, maxDepth, excludes);		
		objDefinition.setElementName("/"+objectTypeId); //specify root element as schema location
//		String sSchema = sbSchema.toString();
//		ProcessingReport report;
		
//		//Validate the schema 
//		ObjectMapper mapper = new ObjectMapper();
//		JsonNode candidateSchema = mapper.readTree(sSchema);
//	    final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
//	    //Even though validSchema not referenced, we want to see if getJsonSchema throws Exception
//        JsonSchema validSchema = factory.getJsonSchema(candidateSchema);
		objDefinition.setJsonSchema(sbSchema.toString());
	}
		
	private void populateQueryFilterSortFields(List<FieldSpecField>fieldSpecFields, String objectTypeId) throws Exception
	{
		JSONArray resourceSearchParams = getResourceSearchParams(objectTypeId);
	    //set up filterable and sortable parameters
		fieldSpecFields.add(new FieldSpecField().withName("_id").withFilterable(true).withSortable(true).withType("string"));
		fieldSpecFields.add(new FieldSpecField().withName("_lastUpdated").withFilterable(true).withSortable(true).withType("date"));
		fieldSpecFields.add(new FieldSpecField().withName("_text").withFilterable(true).withType("string"));
		
		//Read the capabilities JSON for the resource type to get the resource specific fields
		
		for (int x=0; x<resourceSearchParams.length(); x++)
		{
			JSONObject searchParam = ((JSONObject)resourceSearchParams.get(x));
			FieldSpecField filterable = new FieldSpecField().withName(searchParam.getString("name")).withFilterable(true).withSortable(true);
			if (searchParam.has("type"))
			{
				if ("date".equals(searchParam.getString("type")))
					filterable.withType("date");
				else if ("number".equals(searchParam.getString("type")))
					filterable.withType("number");
				else 
					filterable.withType("string"); 
				fieldSpecFields.add(filterable);
			} else {
				logger.warning("No type element found");
			}
		}
	}
	
	private int populateQuerySelectableFields(List<FieldSpecField> fieldSpecFields, String objectTypeId, String stuVersion) throws Exception
	{
		JSONObject schema = FHIRUtilJSON.getJSONSchema(getClass(), stuVersion);

		JSONObject definitions = schema.getJSONObject("definitions");
		JSONObject resourceMetadata = definitions.getJSONObject(objectTypeId);
		if (resourceMetadata.has("allOf"))
		{
			//skip first array element "$ref": "DomainResource#/definitions/DomainResource"
			resourceMetadata = resourceMetadata.getJSONArray("allOf").getJSONObject(1);
		}
		JSONObject resourceProperties = resourceMetadata.getJSONObject("properties");
		Iterator<String> keys = resourceProperties.keys();
		JSONArray selectFields = new JSONArray();
		while(keys.hasNext())
		{
			JSONObject selectField = new JSONObject();
			selectField.put("name", keys.next());
			selectFields.put(selectField);
		}
		
		for (int x=0; x<selectFields.length(); x++)
		{
			JSONObject selectField = ((JSONObject)selectFields.get(x));
			insertSelectable(fieldSpecFields, selectField.getString("name"));
		}
		
		return selectFields.length();
	}
		
	private JSONArray getResourceSearchParams(String resourceName) throws Exception {
		JSONArray searchParams=null;
//		JSONObject metadata = this.getMetadataFromResource();
		JSONObject metadata = this.getConnection().getMetadata();

		JSONObject resourceMetadata = null;
		JSONArray resources = ((JSONObject)metadata.getJSONArray("rest").get(0)).getJSONArray("resource");
		for (int i=0; i<resources.length(); i++)
		{
			if (((JSONObject)resources.get(i)).getString("type").equals(resourceName))
			{
				resourceMetadata = (JSONObject)resources.get(i);
				searchParams = resourceMetadata.getJSONArray("searchParam");
				break;
			}
		} 
		return searchParams;
	}
	
	private static void insertSelectable(List<FieldSpecField> fieldSpecs, String fieldName)
	{
		for(FieldSpecField fieldSpecField : fieldSpecs)
		{
			if (fieldSpecField.getName().contentEquals(fieldName))
			{
				fieldSpecField.setSelectable(true);
				return;
			}
		}
		FieldSpecField fieldSpecField = (new FieldSpecField()).withName(fieldName).withSelectable(true);
		fieldSpecs.add(fieldSpecField);
	}
	
	private static void sortFieldSpecFields(List<FieldSpecField> fieldSpecs)
	{
		//Now sort them
		if (fieldSpecs!=null)
		{
		    Collections.sort(fieldSpecs, new Comparator<FieldSpecField>() {
		        @Override
		        public int compare(FieldSpecField a, FieldSpecField b) {
		            return a.getName().compareToIgnoreCase(b.getName());
		        }
		    });
		}
	}
}