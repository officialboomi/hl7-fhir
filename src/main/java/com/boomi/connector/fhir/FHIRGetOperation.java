// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.fhir;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.HistoryType;
import com.boomi.connector.util.BaseGetOperation;
import com.boomi.util.IOUtil;

/**
 * FHIR GET operation implementation.
 * 
 * @author Dave Hock
 */
public class FHIRGetOperation extends BaseGetOperation {

    public FHIRGetOperation(FHIRConnection conn) {
        super(conn);
    }
//TODO implement maxDocuments/_count per QUery operation
    @Override
    protected void executeGet(GetRequest request, OperationResponse response) {
    	Logger logger = response.getLogger();
        ObjectIdData input = request.getObjectId();
        PropertyMap opProps = this.getContext().getOperationProperties();
        ConnectorCookie cookie = FHIRUtil.getOutputCookie(getContext());

        try {
            // make GET request for the object of the given type and id
        	String url = getConnection().getGetURL(getContext().getObjectTypeId(), input.getObjectId(), opProps, cookie);
            
            if (!HistoryType.none.name().contentEquals(getConnection().getHistory(opProps)))
            {
            	if (Format.json.name().contentEquals(cookie.getFormat()))
            		FHIRUtilJSON.processBatchResponse(url, getConnection(), response, input, opProps, cookie, logger);
            	else
            		FHIRUtilXML.processBatchResponse(url, getConnection(), response, input, opProps, cookie, logger);
            } else {
                FHIRResponse httpResponse = getConnection().doGet(url, cookie);
                if (httpResponse.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                    // A GET request that returns 404 "not found" is considered a success.
                    response.addEmptyResult(input, OperationStatus.SUCCESS, httpResponse.getResponseCodeAsString(),
                            httpResponse.getResponseMessage());
                }
                else {
                    // dump the results into the response
                    InputStream is = null;
                    try {
                        is = httpResponse.getResponse();
                        if (is != null) {
                            response.addResult(input, httpResponse.getStatus(), httpResponse.getResponseCodeAsString(),
                                    httpResponse.getResponseMessage(), ResponseUtil.toPayload(is));
                        }
                        else {
                            response.addEmptyResult(input, httpResponse.getStatus(), httpResponse.getResponseCodeAsString(),
                                    httpResponse.getResponseMessage());
                        }
                    }
                    finally {
                        IOUtil.closeQuietly(is);
                    }
                }	
            }
        }
        catch (Exception e) {
            ResponseUtil.addExceptionFailure(response, input, e);
        }
    }

    @Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}
