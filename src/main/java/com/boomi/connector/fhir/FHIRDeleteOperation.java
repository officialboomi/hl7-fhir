package com.boomi.connector.fhir;

import java.util.logging.Logger;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.util.BaseDeleteOperation;

public class FHIRDeleteOperation extends BaseDeleteOperation {

	protected FHIRDeleteOperation(FHIRConnection conn) {
		super(conn);
	}

	@Override
	protected void executeDelete(DeleteRequest request, OperationResponse response) {
    	Logger logger = response.getLogger();
        String objectType = getContext().getObjectTypeId();

        for (ObjectIdData input : request) {
            try {
                FHIRResponse resp = getConnection().doDelete(objectType, input.getObjectId());
                // just return status values (no content)
                response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                        resp.getResponseMessage());
            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
	}

	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}