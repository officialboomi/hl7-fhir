package com.boomi.connector.fhir;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.GroupingOperator;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.CollectionUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

//Implements the query operations documented here: https://www.hl7.org/fhir/search.html

public class FHIRQueryOperation extends BaseQueryOperation {

	protected FHIRQueryOperation(FHIRConnection conn) {
		super(conn);
	}
	
    /** Enum representing the supported query operations (these names match the ids specified in the connector
    descriptor).  Each operation has a prefix field which is used when sending the query filter to the demo
    service. */
//	private enum QueryOp {
//	    eq("eq"),
//	    NOT_EQUALS("ne"),
//	    GREATER_THAN("gt"),
//	    LESS_THAN("lt"),
//	    GREATER_THAN_OR_EQUALS("ge"),
//	    LESS_THAN_OR_EQUALS("le"),
//	    IN_LIST("in");
//	
//	    private final String _prefix;
//	
//	    private QueryOp(String prefix) {
//	        _prefix = prefix;
//	    }
	
	    
	    /**
	     * Returns the prefix to use for this operation with the demo service.
	     */
//	    public String getPrefix() {
//	        return _prefix;
//	    }
//	}


//    @Override
    protected void executeQuery(QueryRequest request, OperationResponse response) {
       	Logger logger = response.getLogger();
        FilterData input = request.getFilter();
        
        // grab the object type from the context
        String objectType = getContext().getObjectTypeId();

        ConnectorCookie cookie = FHIRUtil.getOutputCookie(getContext());

        String terms="";
        try {

        	if (input!=null && input.getFilter()!=null)
        	{
        		QueryFilter queryFilter = input.getFilter();
        		
	            // convert the sdk filter into url query filter pairs
	            List<Map.Entry<String,String>> baseQueryTerms = constructQueryTerms(queryFilter);
	            
	            // process all the results, handling batching
	            List<Map.Entry<String,String>> filterTerms = null; 
	            
	            if (baseQueryTerms!=null)
	            {
	            	filterTerms = new ArrayList<Map.Entry<String,String>>(baseQueryTerms);
		            
		        	for (int x=0; x<filterTerms.size(); x++)
		        	{
		        		Map.Entry<String, String> item = filterTerms.get(x);

		        		terms+="&"+item.getKey()+"=";
		        		
		        		terms+=item.getValue();
		        	}
	            }
	        	terms += getSortTermsURI(queryFilter);
        	}
        	terms += getSelectTermsURI(cookie.getTotalNumberFields());
        	
            String extraURIParams = this.getContext().getOperationProperties().getProperty(FHIRConnector.OperationPropertyIds.extraURIParams.name());
            if (extraURIParams!=null)
            	terms+=extraURIParams;
            
//TODO this needs to be for all items? Or at least GET????
            
            PropertyMap opProps = this.getContext().getOperationProperties();
        	terms+="&_count=" + getConnection().getBatchSize(opProps);
                        	
        	String url = getConnection().getQueryURL(terms, objectType, cookie);
           	logger.log(Level.INFO, "terms:" + terms + " maxDocuments:" + this.getConnection().getMaxDocuments(opProps));
            if (Format.xml.name().contentEquals(cookie.getFormat()))
            {
            	FHIRUtilXML.processBatchResponse(url, getConnection(), response, input, opProps, cookie, logger);
            }
            else
            {
            	FHIRUtilJSON.processBatchResponse(url, getConnection(), response, input, opProps, cookie, logger);
            }
            	
        } catch(Exception e) {
        	FHIRUtil.handleException(e, logger);
            ResponseUtil.addExceptionFailure(response, input, e);
        }
    }
    
    String getSelectTermsURI(int totalNumberFields)
    {
    	StringBuilder terms= new StringBuilder();
    	List<String> selected = getContext().getSelectedFields();
    	
    	if (selected!=null && selected.size()>0 && selected.size()!=totalNumberFields)
    	{
    		terms.append("&_elements=");
    		for (int x=0; x < selected.size(); x++)
    		{
    			if (x!=0) terms.append(",");
    				terms.append(selected.get(x));
    		}
    	}  	
    	return terms.toString();
    }
    
    static String getSortTermsURI(QueryFilter queryFilter)
    {
    	String sortTermsString="";
    	List<Sort> sortTerms = queryFilter.getSort();

    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()==0)
        			{
        				sortTermsString+="&_sort=";
        			} else {
        				sortTermsString+=",";
        			}
        			if (sort.getSortOrder()=="desc")
        				sortTermsString+="-";
        			sortTermsString += sortTerm;
        		}
        	}
    	}
    	return sortTermsString;
    }
    
    /**
     * Constructs a list of query filter terms from the given filter, may be {@code null} or empty.
     *
     * @param filter query filter from which to construct the terms
     *
     * @return collection of query filter terms for the demo service
     */
    static List<Map.Entry<String,String>> constructQueryTerms(QueryFilter filter) {
        if((filter == null) || (filter.getExpression() == null)) {
            // no filter given, (this is equivalent to "select all")
            return null;
        }

        List<Map.Entry<String,String>> terms = new ArrayList<Map.Entry<String,String>>();

        // see if base expression is a single expression or a grouping expression
        Expression baseExpr = filter.getExpression();
        if (baseExpr!=null)
        {
            if(baseExpr instanceof SimpleExpression) {
                
                // base expression is a single simple expression
                terms.add(constructSimpleExpression((SimpleExpression)baseExpr));
                
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // we only support "AND" groupings
                if(groupExpr.getOperator() != GroupingOperator.AND) {
                    throw new IllegalStateException("Invalid grouping operator " + groupExpr.getOperator());
                }

                // parse all the simple expressions in the group
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                    if(!(nestedExpr instanceof SimpleExpression)) {
                        throw new IllegalStateException("Only one level of grouping supported");
                    }
                    terms.add(constructSimpleExpression((SimpleExpression)nestedExpr));
                }
            }
        }
        
        return terms;
    }

    /**
     * Returns a url query term (key, value pair) constructed from the given SimpleExpression.
     *
     * @param expr the simple expression from which to construct the term
     *
     * @return url query filter term for the demo service
     */
    static Map.Entry<String,String> constructSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
        String propName = expr.getProperty();
        String operator = expr.getOperator();

        // translate the operation id into one of our supported operations
//        QueryOp queryOp = QueryOp.valueOf(expr.getOperator());

        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) {
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");
        }

        // this is the single operation argument
        String queryValue = "";
        
        if (expr.getArguments()!=null && expr.getArguments().size()!=0)
        	queryValue=expr.getArguments().get(0);

        // combine the property name and operation into the query filter key
        //handle : operators. Note we can't use : as a prefix in the descriptor so we substitute here 
        //For these operators, the operation is appended to the field name (vs. prepended to the operand)
        if (operator.startsWith("_"))
        	propName = propName + operator.replace('_', ':');
        else if (operator.contentEquals("isnull"))
        {
        	propName = propName+":missing";
        	queryValue = "true";
        }
        else if (operator.contentEquals("isnotnull"))
        {
        	propName = propName+":missing";
        	queryValue = "false";
        }
        else if (!operator.contentEquals("eq"))
        	queryValue = operator + queryValue;

        return new AbstractMap.SimpleEntry<String, String>(propName, queryValue);
    }

	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}