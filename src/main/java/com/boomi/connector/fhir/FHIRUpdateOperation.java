package com.boomi.connector.fhir;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.DOMUtil;
import com.boomi.util.IOUtil;

public class FHIRUpdateOperation extends BaseUpdateOperation {

	protected FHIRUpdateOperation(FHIRConnection conn) {
		super(conn);
	}
	
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		doUpdate(request, response, getContext(), getConnection());
	}
	
	public static void doUpdate(UpdateRequest request, OperationResponse response, OperationContext context, FHIRConnection connection) {
    	Logger logger = response.getLogger();
        String objectType = context.getObjectTypeId();
        PropertyMap opProps = context.getOperationProperties();
        ConnectorCookie cookie = FHIRUtil.getInputCookie(context);

        for (ObjectData input : request) {

            try {
                // POST the content to the create url
            	InputStream isData = input.getData();

            	//Scan the input to get id for restful URI 
            	String id=null;
            	String payload="";
            	if (cookie.getFormat().contentEquals(FHIRConnector.Format.json.name()))
				{
                	JSONObject json = new JSONObject(new JSONTokener(isData));
                	//TODO can we avoid the json.toString().getBytes()->InputStream and go directly to a stream?
                	id = json.getString("id");
                	payload = json.toString();
				} else {
					//TODO get id from the XML Payload
		            Document doc = DOMUtil.newDocumentBuilderNS().parse(isData);
		            Element top = doc.getDocumentElement();
		            NodeList items = top.getElementsByTagName("id");
		            if (items!=null && items.getLength()==1)
		            {
		            	Node idNode = items.item(0);
		            	Node valueNode = idNode.getAttributes().getNamedItem("value");
		            	if (valueNode!=null)
		            		id=valueNode.getNodeValue();
		            }
				}
	            if (id==null || id.length()==0)
	            	throw new ConnectorException("id is a required field and cannot be null");
            	isData = new ByteArrayInputStream(payload.getBytes());
                FHIRResponse resp = connection.doUpdate(objectType, id, isData, opProps, cookie);

                // dump the results into the response
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {
                        response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                        response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage());
                    }
                }
                finally {
                    IOUtil.closeQuietly(obj);
                }
            }
            catch (Exception e) {
                // make best effort to process every input
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
	}

//	static void doUpdate

	@Override
    public FHIRConnection getConnection() {
        return (FHIRConnection) super.getConnection();
    }
}