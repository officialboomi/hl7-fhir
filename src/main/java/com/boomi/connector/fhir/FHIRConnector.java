// Copyright (c) 2019 Boomi, Inc.
package com.boomi.connector.fhir;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

/**
 * FHIRConnector operation implementation.
 * 
 * @author Dave Hock
 */
public class FHIRConnector extends BaseConnector {

    public enum ConnectionPropertyIds {
    	url,alternateUrl,username,password,authenticationType,headerProperties
    }
    
    public enum OperationPropertyIds {
    	replace, format, history, maxDocuments, batchSize, editResponseType, stuVersion, maxDepth, excludeExtensions, extraURIParams
    }
    
    public enum StuVersion {
    	STU3, STU4, R5
    }
    
	public enum EditResponseType {
		minimal,representation,OperationOutcome
	}
	
	public enum Format {
		json,xml
	}  

	public enum AuthType {
		none,basic,oauth
	}  
	
	public enum HistoryType {
		none,resource,record
	}

	public enum HttpMethods {
		POST, PUT, PATCH, DELETE
	}
	//TODO Implement XMLResponseSplitter when we handle complex "next" elements like:
	//<link><relation value="next"/><url value="http://hapi.fhir.org/baseR4?_getpages=de56821e-901b-4cf8-b9f5-9375e7c2d3e6&amp;_getpagesoffset=20&amp;_count=20&amp;_format=JSON&amp;_pretty=true&amp;_bundletype=searchset"/>
	public static final String RESPONSE_NEXT_PAGE_ELEMENT_NAME = ""; 
	public static final String RESPONSE_LIST_ELEMENT_NAME = "resource";
    @Override
    public Browser createBrowser(BrowseContext context) {
        return new FHIRBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new FHIRGetOperation(createConnection(context));
    }

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new FHIRQueryOperation(createConnection(context));
    }

    @Override
    protected Operation createCreateOperation(OperationContext context) {
        return new FHIRCreateOperation(createConnection(context));
    }

    @Override
    protected Operation createUpdateOperation(OperationContext context) {
        return new FHIRUpdateOperation(createConnection(context));
    }

    @Override
    protected Operation createUpsertOperation(OperationContext context) {
        return new FHIRUpsertOperation(createConnection(context));
    }
   
    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        return new FHIRDeleteOperation(createConnection(context));
    }
   
    private FHIRConnection createConnection(BrowseContext context) {
        return new FHIRConnection(context);
    }
}