package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.stream.XMLStreamException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.TrackedData;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

public class FHIRUtilJSON
{	
    static void processBatchResponse(String nextPageURL, FHIRConnection connection, OperationResponse opResponse, TrackedData input, PropertyMap opProps, ConnectorCookie cookie, Logger logger) throws XMLStreamException, IOException
    {
        // process all the results, handling batching
        int numPages = 0;
        do {            
           	logger.log(Level.INFO, "Document index:" + numPages);
           	FHIRResponse resp = connection.doGet(nextPageURL, cookie);
                        
            if (resp.getStatus() != OperationStatus.SUCCESS) {
                // just dump the result and bailout, nothing more we can do
                InputStream obj = null;
                try {
                    obj = resp.getResponse();
                    if (obj != null) {

                        opResponse.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                        		resp.getResponseMessage(), ResponseUtil.toPayload(obj));
                    }
                    else {
                    	opResponse.addPartialResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
                                resp.getResponseMessage(), null);
                    }
                }
                finally {
                    IOUtil.closeQuietly(obj);
                }
                break;
            }

            JSONResponseSplitter respSplitter = null;
            try {

                // split "list" result document into multiple payloads and process each as a partial result
//                respSplitter = new JsonArraySplitter(resp.getResponse(), "entry");

                respSplitter = new JSONResponseSplitter(resp.getResponse());

                for(Payload p : respSplitter) {
                    ResponseUtil.addPartialSuccess(opResponse, input, resp.getResponseCodeAsString(), p);
                }
                //We do maxpages instead of maxdocuments because IOUtil.closeQuietly locks up if we break out of forloop above
                nextPageURL = respSplitter.getNextPageURL();
                
            } finally {
                IOUtil.closeQuietly(respSplitter);
            }
            numPages++;
        } while(!StringUtil.isBlank(nextPageURL) && numPages < connection.getMaxDocuments(opProps));

        // indicate that we are finished adding all the results
        opResponse.finishPartialResult(input);
    }        	
	
	//Read schema from static resource
	public static JSONObject getJSONSchema(Class theClass, String stuVersion) throws Exception   {
		return new JSONObject(new JSONTokener(theClass.getClassLoader().getResourceAsStream("resources/"+stuVersion+"/json/fhir.schema.json")));
	}
	
    /************************************************************************************/
    /* These methods resolve all $ref's to work around Boomi's lack of JSON recursive Type support*/
    /************************************************************************************/
	//TODO option to exclude extension elements	
	
	
	public static void resolveObjectSchema(StringBuilder sbSchema, JSONObject schema, String objectTypeId, long maxDepth, List<String>excludes) throws JsonProcessingException, IOException
	{
		JSONObject definitions = schema.getJSONObject("definitions");
		JSONObject resourceType = definitions.getJSONObject(objectTypeId);

		sbSchema.append("{");
		sbSchema.append("\"$schema\": \"http://json-schema.org/draft-04/schema#\",\""+objectTypeId+"\":{");
		resolveObjectSchemaRecursive(sbSchema,definitions,objectTypeId,resourceType, 0, maxDepth, excludes);
		sbSchema.append("}}");
	}
	
	public static void appendComma(StringBuilder sb)
	{
		if (sb.lastIndexOf("{")!=sb.length()-1 && sb.lastIndexOf(",")!=sb.length()-1)
		{
			sb.append(',');
		}
	}
	public static void resolveObjectSchemaRecursive(StringBuilder sb, JSONObject definitions, String title, JSONObject parentType, int depth, long maxDepth, List<String> excludes)
	{
		if (depth>maxDepth)
		{
			//just type it as a string and give up
			sb.append("\"type\":\"string\"");
			return;
		}
		if (parentType.has("allOf"))
		{
			//skip first array element "$ref": "DomainResource#/definitions/DomainResource"
			JSONArray allOf = parentType.getJSONArray("allOf");
			parentType = allOf.getJSONObject(allOf.length()-1);
		}

		JSONObject properties = parentType.getJSONObject("properties");
		sb.append("\"type\":\"object\",");
		sb.append("\"properties\": {");

		if (properties!=null)
		{
			Iterator iter = properties.keys();
			while (iter.hasNext())
			{				
				String key = (String)iter.next();
				JSONObject property = properties.getJSONObject(key);
				resolveProperty(sb, definitions, key, property, depth+1, maxDepth, false, excludes);
				if (iter.hasNext())
				{
					appendComma(sb);
				}
			}
		}		
		//TODO we don't know if all following nodes will be filtered. If so, we end up with a dangling comma.
		//I do hate this kludge
		if (sb.charAt(sb.length()-1)==',')
			sb.setLength(sb.length()-1);
		sb.append("}");
	}
	public static void resolveProperty(StringBuilder sb, JSONObject definitions, String key, JSONObject property, int depth, long maxDepth, boolean insideArray, List<String> excludes)
	{
		if (property.has("$ref")) 
		{
			String ref = property.getString("$ref");
			String subTypeName = ref.split("/")[2];
			JSONObject subType = definitions.getJSONObject(subTypeName);
			resolveProperty(sb, definitions, key, subType, depth+1, maxDepth, insideArray, excludes);
		} else if ((!excludes.contains(key) && (!excludes.contains("extension") || !key.startsWith("_")))){
			if (!insideArray)
				sb.append("\""+key+"\": {");
			if (property.has("type"))
			{
				String type = property.getString("type");
				if (type.contentEquals("date") || type.contentEquals("dateTime"))
					type="date";
				sb.append("\"type\":\""+type+"\"");
				
				if (type.contentEquals("array"))
				{
					appendComma(sb);
					if (property.has("items"))
					{	
						sb.append("\"items\": {");
						JSONObject subType = property.getJSONObject("items");
						if (subType.has("properties"))
						{
							resolveObjectSchemaRecursive(sb, definitions, key, subType, depth+2, maxDepth, excludes);
						}
						else
						{
							resolveProperty(sb, definitions, key, subType, depth+2, maxDepth, true, excludes);							
						}
						sb.append("}");
					}
				}
			} else if (property.has("properties") || property.has("allOf")) {
				resolveObjectSchemaRecursive(sb,definitions, key, property, depth+1, maxDepth, excludes);
			} else {
				//just type it as a string and give up
				sb.append("\"type\":\"string\"");
			}
			if (!insideArray)
				sb.append("}");
		}
	}
}