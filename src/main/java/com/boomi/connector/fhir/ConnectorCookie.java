package com.boomi.connector.fhir;

import com.boomi.connector.fhir.FHIRConnector.EditResponseType;
import com.boomi.connector.fhir.FHIRConnector.Format;

public class ConnectorCookie {
		
	private String format = Format.json.name();
	private int totalNumberFields = 0;
	private String representation = EditResponseType.OperationOutcome.name();
	private String stuVersion = "R5";

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getTotalNumberFields() {
		return totalNumberFields;
	}

	public void setTotalNumberFields(int totalNumberFields) {
		this.totalNumberFields = totalNumberFields;
	}

	public String getRepresentation() {
		return representation;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public String getStuVersion() {
		return stuVersion;
	}

	public void setStuVersion(String stuVersion) {
		this.stuVersion = stuVersion;
	}
}
