// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.fhir;

import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.boomi.connector.util.xml.XMLSplitter;

/**
 * XMLSplitter for multiple object responses from the demo service.  Splits the xml document with a "list" root
 * element into multiple objects.  Also stores any returned next "offset" attribute from the response.
 *
 * @author James Ahlborn
 */

//TODO how does splitter handle complex path like....
// /Bundle/entry , /Bundle/link[relation/@value='next']/url/@value

public class XMLResponseSplitter extends XMLSplitter
{
    private String _nextURL;
   
    public XMLResponseSplitter(InputStream in)
        throws XMLStreamException
    {
        super(createInputFactory().createXMLEventReader(in));
    }

    /**
     * Returns any next "offset" attribute included in the response.  Not valid until the response has been consumed.
     */
    public String getNextPageURL()
    {
        return _nextURL;
    }
    
    @Override
    protected XMLEvent findNextObjectStart(boolean isFirst) throws XMLStreamException
    {
// 		find /link[relation/@value=next]/url/@value
//	    <link>
//        <relation value="next"/>
//        <url value="http://hapi.fhir.org/baseR4?_getpages=c896d0c9-7ec4-4216-86e0-9d330c69a013&amp;_getpagesoffset=20&amp;_count=20&amp;_format=xml&amp;_pretty=true&amp;_bundletype=searchset"/>
//     </link>
//     <entry>
//        <fullUrl value="http://hapi.fhir.org/baseR4/Patient/601494"/>
//        <resource>
//           <Patient xmlns="http://hl7.org/fhir">
//              <id value="601494"/>

    	StartElement element=null;
    	boolean isInNextLink=false;
    	boolean isInLink=false;
        element = findNextElementStart(getReader());
        while (element!=null)
        {
            if (element==null)
            	break;
        	if (_nextURL==null)
        	{
            	if (isElementName(element, "link"))
    			{
            		isInLink=true;
    			} 
            	else if (isInLink && isElementName(element, "relation"))
    			{
      	           Attribute attr = element.getAttributeByName(new QName("value"));
      	           if (attr!=null && attr.getValue().contentEquals("next"))
      	           {
      	        	   isInNextLink=true;
      	           } 
      	           else
      	        	   isInLink=false;
    			}
            	else if (isInNextLink && isElementName(element, "url"))
            	{
       	           Attribute attr = element.getAttributeByName(new QName("value"));
       	           _nextURL = attr.getValue();
            	} 
        	}             	
        	if (isElementName(element, FHIRConnector.RESPONSE_LIST_ELEMENT_NAME)) 
        	{
        		return findNextElementStart(getReader()); //get resource element which is child of resource 
        	}
            element = findNextElementStart(getReader());
        }
        // skip to the next start-element event
        return null;
    }    

	boolean isElementName(StartElement element, String candidate)
	{
		return element.getName().getLocalPart().contentEquals(candidate);
	}
}
