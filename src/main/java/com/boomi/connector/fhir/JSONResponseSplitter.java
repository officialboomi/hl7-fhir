package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import com.boomi.util.json.splitter.JsonSplitter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JSONResponseSplitter extends JsonSplitter{
    private String _nextURL;

	protected JSONResponseSplitter(InputStream inputStream) throws IOException {
		super(inputStream);
//		JsonArraySplitter as = new JsonArraySplitter(inputStream,"");
//		JsonRootArraySplitter as2 = new JsonRootArraySplitter(inputStream);
//		as2.
	}

    public String getNextPageURL()
    {
        return _nextURL;
    }
    
	@Override
	protected JsonToken findNextNodeStart() throws IOException {
		JsonParser jsonParser = this.getParser();
    	JsonToken element=null;
    	boolean isInNextLink=false;
    	boolean isInLink=false;

//We want to parse all the entries but grab the resource child object.
//    	"entry": [ {
//    	    "fullUrl": "http://hapi.fhir.org/baseR4/Patient/1092472",
//    	    "resource": {
//    	      "resourceType": "Patient",
    		
    	element = jsonParser.nextToken();
        while (element!=null)
        {
			String name = jsonParser.getCurrentName();
			if (name!=null)
			{
	        	if (_nextURL==null)
	        	{
	            	if ("link".contentEquals(name))
	    			{
	            		isInLink=true;
	    			} 
	            	else if (isInLink && "relation".contentEquals(name) && element == JsonToken.VALUE_STRING)
	    			{
	            		String value = jsonParser.getValueAsString();
	            		if (value!=null && "next".contentEquals(value))
	      	           {
	      	        	   isInNextLink=true;
	      	           } 
	    			}
	            	else if (isInNextLink && "url".contentEquals(name) && element == JsonToken.VALUE_STRING)
	            	{
	            		_nextURL = jsonParser.getValueAsString();
	            	} 
	        	}             	
	        	if (FHIRConnector.RESPONSE_LIST_ELEMENT_NAME.contentEquals(name)) 
	        	{
	        		return jsonParser.nextToken();
	        	}
			}
        	element = jsonParser.nextToken();
		}		
		//We are now pointing to entry but we need to drill down to entry/resource
//    	if (startElement!=JsonToken.START_OBJECT)
//    	{
//    		String name = jsonParser.getCurrentName();
//    		startElement=null;
//    	}
        return null;
	}
}
