package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationContext;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FHIRUtil
{	
     
    static InputStream getResourceAsStream(String resourcePath, Class theClass) throws Exception
    {
    	InputStream is = null;
		try {
			is = theClass.getClassLoader().getResourceAsStream(resourcePath);			
		} catch (Exception e)
		{
			throw new Exception("Error loading resource: "+resourcePath + " " + e.getMessage());
		}
		if (is==null)
			throw new Exception("Error loading resource: "+resourcePath);
		return is;
    }
	
	static void handleException(Exception e, Logger logger)
	{
		if (logger!=null)
		{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
		   	logger.log(Level.SEVERE, e.toString() + " " + sw.toString());
		}
	}
	
	static ConnectorCookie getInputCookie(OperationContext context)
	{
        ObjectMapper mapper = new ObjectMapper();
    	
    	ConnectorCookie cookie = new ConnectorCookie();
		try {
			cookie = mapper.readValue(context.getObjectDefinitionCookie(ObjectDefinitionRole.INPUT),ConnectorCookie.class);
		} catch (Exception e1) {
			//If cookie not there, just keep the defaults
		}
    	return cookie;
	}
	
	static ConnectorCookie getOutputCookie(OperationContext context)
	{
        ObjectMapper mapper = new ObjectMapper();
    	
    	ConnectorCookie cookie = new ConnectorCookie();
		try {
			cookie = mapper.readValue(context.getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT),ConnectorCookie.class);
		} catch (Exception e1) {
			//If cookie not there, just keep the defaults
		}
    	return cookie;
	}
}