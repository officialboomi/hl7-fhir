// Copyright (c) 2019 Boomi, Inc.
package com.boomi.connector.fhir;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.OAuth2Token;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.connector.fhir.FHIRConnector.AuthType;
import com.boomi.connector.fhir.FHIRConnector.ConnectionPropertyIds;
import com.boomi.connector.fhir.FHIRConnector.Format;
import com.boomi.connector.fhir.FHIRConnector.HistoryType;
import com.boomi.connector.fhir.FHIRConnector.HttpMethods;
import com.boomi.connector.fhir.FHIRConnector.OperationPropertyIds;

/**
 * FHIRConnection operation implementation.
 * 
 * @author Dave Hock
 */
public class FHIRConnection extends BaseConnection {
//    /** property which users can use to change the base url */
//    public static final String URL_PROPERTY = "url";
//    public static final String ALT_URL_PROPERTY = "alternateUrl";
//    public static final String AUTHTYPE_PROPERTY = "authenticationType";
//    public static final String USERNAME_PROPERTY = "username";
//    public static final String PASSWORD_PROPERTY = "password";
//    public static final String REPLACE_PROPERTY = "replace";
//    public static final String FORMAT_PROPERTY = "format";
//    public static final String HISTORY_PROPERTY = "history";
//    public static final String MAXDOCUMENTS_PROPERTY = "maxDocuments";
//    public static final String BATCHSIZE_PROPERTY = "batchSize";
//    public static final String EDITRESPONSETYPE_PROPERTY = "editResponseType";
//    public static final String STUVERSION_PROPERTY = "stuVersion";
//    
//    /** string representing an http POST method */
//    private static final String POST_METHOD = "POST";
//    /** string representing an http PUT method */
//    private static final String PUT_METHOD = "PUT";
//    private static final String PATCH_METHOD = "PATCH";
//    /** string representing an http DELETE method */
//    private static final String DELETE_METHOD = "DELETE";
    /** property name for the http content type header */
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    /** header property value for http content containing XML data */
    private static final String JSON_CONTENT_TYPE = "application/json"; //TODO /fhir+json?
    private static final String XML_CONTENT_TYPE = "application/xml"; //TODO /fhir+xml?
    
    private final String _baseUrl;
    private String _authenticationType;
    private final String _username;
    private final String _password;
    private final Logger logger;

    public FHIRConnection(BrowseContext context) {
        super(context);
        PropertyMap connProps = context.getConnectionProperties();
        _baseUrl = getBaseUrl();
        _authenticationType = connProps.getProperty(ConnectionPropertyIds.authenticationType.name(), AuthType.none.name());
        _username = connProps.getProperty(ConnectionPropertyIds.username.name());
        _password = connProps.getProperty(ConnectionPropertyIds.password.name());
        logger = Logger.getLogger(this.getClass().getName());
    }

    public String getBaseUrl() {
        PropertyMap connProps = this.getContext().getConnectionProperties();
    	String altUrl = connProps.getProperty(ConnectionPropertyIds.alternateUrl.name());
    	if (altUrl != null && altUrl.length()>0)
    		return altUrl;
        return connProps.getProperty(ConnectionPropertyIds.url.name());
    }
    
    private String buildUrl(String objectType, String objectId) throws Exception
    {
        if (_baseUrl == null || !_baseUrl.startsWith("http"))
        	throw new Exception("A valid URL protocol must be specified in the connection");
    	String url = _baseUrl + "/" + objectType;
    	if (objectId != null && !objectId.isEmpty())
    		url+="/"+objectId;

    	return url;
    }
    
    public String getGetURL(String objectType, String objectId, PropertyMap opProps, ConnectorCookie cookie) throws Exception
    {
        String url = buildUrl(objectType, objectId);
        String history = opProps.getProperty(OperationPropertyIds.history.name(), HistoryType.none.name());
        String urlSuffix = "";
    	if (history!=null)
    	{
    		if (HistoryType.record.name().contentEquals(history))
    		{
    			urlSuffix="/_history";
    		}
    		else if (HistoryType.resource.name().contentEquals(history))
    		{
    			urlSuffix="/_history";
    			objectId="";
    		}
    	}
        url += urlSuffix;
    	url += getFormatURI(cookie.getFormat());
    	logger.log(Level.INFO, "Executing GET: " + url);
    	
        return url;
    }
    
    public FHIRResponse doGet(String url, ConnectorCookie cookie) throws IOException
    {
   		logger.log(Level.INFO, "Executing Pagination GET: " + url);
        return new FHIRResponse((HttpURLConnection) openConnection(new URL(url), cookie));
    }  

    public void testConnection() throws Exception
    {
    	FHIRResponse response = this.doGet(_baseUrl+"/metadata?format=json", null);
    	if (200 != response.getResponseCode())
    		throw new Exception("Problem connecting to endpoint: " + response.getResponseCode() + " " + response.getResponseMessage());   			
    }
    
	public JSONObject getMetadata() throws Exception 
	{
		String url = getBaseUrl()+"/metadata?_format=json"; //We always want metadata in JSON
        logger.log(Level.INFO, "Getting metadata: " + url);

        HttpURLConnection conn = (HttpURLConnection) openConnection(new URL(url), null);
		FHIRResponse response = new FHIRResponse(conn);
    	if (200 != response.getResponseCode())
    		throw new Exception("Problem reading metadata from connection: " + response.getResponseCode() + " " + response.getResponseMessage());
		JSONObject root = new JSONObject(new JSONTokener(response.getResponse()));
		return root;
	}
    
	
	
    public String getQueryURL(String terms, String objectType, ConnectorCookie cookie) throws Exception
    {
    	String urlString = buildUrl(objectType, null) + getFormatURI(cookie.getFormat());
    	urlString+=terms;

    	logger.log(Level.INFO,"doQuery:"+urlString);
    	return urlString;
    }

    /**
     * Updates the given objects
     * 
     * @param replace whether or not to do complete replacement
     * @param objectType id of the object type
     * @param inputBatch batch of objects to update
     * @return response from the demo service
     * @throws Exception 
     * @throws XMLStreamException
     */
//    public FHIRResponse doUpdateBatch(boolean replace, String objectType, List<ObjectData> inputBatch) throws IOException,
//            XMLStreamException {
//        // construct batch request url for this object
//    	String url = _baseUrl + "/" + objectType + "/" + objectId;
//    	url += "?_format=json";
//
//        // POST (partial update) or PUT (complete replacement) the content to
//        // the update url
//        String requestMethod = (replace ? PUT_METHOD : POST_METHOD);
//        OutputStream out = null;
//        DemoRequestJoiner reqJoiner = null;
//        try {
//
//            HttpURLConnection conn = prepareSend(url, requestMethod, JSON_CONTENT_TYPE);
//            out = conn.getOutputStream();
//            out.write(inputBatch.get(0).getData());
//
//            // join the current batch into one output xml document, wrapped with
//            // a "list" element
//            reqJoiner = new DemoRequestJoiner(out);
//            reqJoiner.writeAll(inputBatch);
//            reqJoiner.close();
//            out.close();
//
//            return new FHIRResponse(conn);
//        }
//        finally {
//            IOUtil.closeQuietly(reqJoiner, out);
//        }
//    }

    /**
     * Creates the given object
     * 
     * @param objectType id of the object type
     * @param data object to create
     * @return response from demo service
     * @throws Exception 
     */
    public FHIRResponse doCreate(String objectType, String objectId, InputStream data,  PropertyMap opProps, ConnectorCookie cookie) throws Exception {
    	String url = buildUrl(objectType, objectId);
    	if (objectId != null && !objectId.isEmpty())
    		url+="/"+objectId;
    	url += getFormatURI(cookie.getFormat());
        logger.log(Level.INFO, "Executing CREATE: " + url);
    	
        return new FHIRResponse(send(new URL(url), HttpMethods.POST.name(), opProps, cookie, data));
    }

    public FHIRResponse doUpdate(String objectType, String objectId, InputStream data, PropertyMap opProps, ConnectorCookie cookie) throws Exception {
    	String url = buildUrl(objectType, objectId)+"/"+ getFormatURI(cookie.getFormat());
        String requestMethod = HttpMethods.PATCH.name();
        if (opProps!=null)
        {
        	if (opProps.get(OperationPropertyIds.replace.name())!=null && (boolean)opProps.get(OperationPropertyIds.replace.name()))
        	{
        		requestMethod = HttpMethods.PUT.name();
        	}
        }
        logger.log(Level.INFO, "Executing UPDATE " + requestMethod + ":" + url);
        return new FHIRResponse(send(new URL(url), requestMethod, opProps, cookie, data));
    }
    
    /**
     * Deletes the object with the given id 
     * 
     * @param objectType id of the object type
     * @param objectId id of the object to delete
     * @return response from the demo service
     * @throws Exception 
     */
    public FHIRResponse doDelete(String objectType, String objectId) throws Exception {
    	String url = buildUrl(objectType, objectId);
        logger.log(Level.INFO, "Executing DELETE: " + url);
        HttpURLConnection conn = (HttpURLConnection) openConnection(new URL(url), null);
        conn.setRequestMethod(HttpMethods.DELETE.name());

        return new FHIRResponse(conn);
    }
    
    private String getFormatURI(String format)
    {
    	return ("?_format="+format);
    }
    
    private String getContentType(String format)
    {
    	String contentType = XML_CONTENT_TYPE;
       	if (Format.json.name().contentEquals(format))
        		contentType = JSON_CONTENT_TYPE;
    	return contentType;
    }
    
    long getMaxDocuments(PropertyMap opProps)
    {
    	return opProps.getLongProperty(OperationPropertyIds.maxDocuments.name(), -1L);
    }

    long getBatchSize(PropertyMap opProps)
    {
        return opProps.getLongProperty(OperationPropertyIds.batchSize.name(), 20L);
    }
        
    String getHistory(PropertyMap opProps)
    {
        return opProps.getProperty(OperationPropertyIds.history.name(), HistoryType.none.name());
    }
    
    Map<String, String> getCustomHeaders()
    {
    	return this.getContext().getConnectionProperties().getCustomProperties(ConnectionPropertyIds.headerProperties.name());
    }
        
    private HttpURLConnection send(URL url, String requestMethod, PropertyMap opProps, ConnectorCookie cookie, InputStream data)
            throws IOException {
        try {
            HttpURLConnection conn = prepareSend(url, requestMethod, opProps, cookie);
            OutputStream out = conn.getOutputStream();
            try {
                StreamUtil.copy(data, out);
            }
            finally {
                out.close();
            }

            return conn;
        }
        finally {
            IOUtil.closeQuietly(data);
        }
    }
    
    private HttpURLConnection prepareSend(URL url, String requestMethod, PropertyMap opProps, ConnectorCookie cookie) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) openConnection(url, cookie);
        conn.setRequestMethod(requestMethod);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestProperty(CONTENT_TYPE_HEADER, getContentType(cookie.getFormat()));
        return conn;
    }

    private HttpURLConnection openConnection(URL url, ConnectorCookie cookie) throws IOException
    {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    	if (AuthType.basic.name().contentEquals(_authenticationType))
    	{
        	String userpass = _username + ":" + _password;
        	String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
        	connection.setRequestProperty ("Authorization", basicAuth);
    	} else if (AuthType.oauth.name().contentEquals(_authenticationType)) {
    		OAuth2Context oAuth2Context = getContext().getConnectionProperties().getOAuth2Context("oauthOptions");
    		OAuth2Token oAuth2Token = oAuth2Context.getOAuth2Token(false);
    		String accessToken = oAuth2Token.getAccessToken();
        	String oAuth = "Bearer " + accessToken;
           	connection.setRequestProperty ("Authorization", oAuth);
       	}
    	if (cookie!=null)
    		connection.setRequestProperty ("Prefer", "return="+cookie.getRepresentation());
        Map<String,String> headers = this.getCustomHeaders();
        for(String header:headers.keySet())
        {
        	connection.setRequestProperty(header, headers.get(header));
        }

    	return connection;
    }
}